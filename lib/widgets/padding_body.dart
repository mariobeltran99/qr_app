import 'package:flutter/cupertino.dart';

Padding paddingBody({List<Widget> wigets, MainAxisAlignment mainAxis, CrossAxisAlignment crossAxis}) {
  return Padding(
    padding: EdgeInsets.all(20),
    child: Column(
      mainAxisAlignment: mainAxis ?? MainAxisAlignment.start,
      crossAxisAlignment: crossAxis ?? CrossAxisAlignment.start,
      children: wigets,
    ),
  );
}
