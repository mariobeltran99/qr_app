import 'package:flutter/material.dart';

RaisedButton button({String text, Function callback}) {
  return RaisedButton(
    child: Text(text),
    onPressed: callback,
  );
}
