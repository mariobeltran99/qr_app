import 'package:flutter/material.dart';
import 'modules/scannerqr/scannerqr_state.dart';

void main() => runApp(ScannerApp());

class ScannerApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: {
        '/': (context) => new ScannerQrState()
      },
    );
  }
}
