import 'package:flutter/material.dart';
import 'package:parkingqr/widgets/button.dart';

String buttonText = 'Scan QR';

List<Widget> whenUserNotScan(Function startScan) {
  return <Widget>[
    Center(
      child: button(
        text: buttonText,
        callback: startScan
      ),
    )
  ];
}

List<Widget> whenUserScan(String data, Function startScan) {
  return <Widget> [
    Center(
      child: Text(
        data,
        style: TextStyle(
          fontSize: 48,
          fontWeight: FontWeight.bold
        ),
      ),
    ),
    Text('\n\n'),
    Center(
      child: button(
        text: buttonText,
        callback: startScan
      ),
    )
  ];
}