import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:parkingqr/widgets/padding_body.dart';
import 'package:parkingqr/modules/scannerqr/scannerqr_scaffolds.dart';
import 'package:parkingqr/modules/scannerqr/scannerqr_state.dart';
import 'package:qrscan/qrscan.dart' as scanner;

class ScannerQrWidget extends State<ScannerQrState> {
  bool flagScanner = false;
  String qrContent;

  void onScannerStart() async {
    String data = await scanner.scan();
    setState(() {
      qrContent = data;
      changeFlagState();
    });
  }

  void changeFlagState() {
    flagScanner = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: paddingBody(
        wigets: !flagScanner 
          ? whenUserNotScan(onScannerStart)
          : whenUserScan(qrContent, onScannerStart),
        crossAxis: CrossAxisAlignment.center,
        mainAxis: MainAxisAlignment.center
      )
    );
  }
}
